require "date"
@start = Date.today
@last = @start - 10
@start.step(@last,-1) do |s| 
  puts s.strftime("%Y-%m-%d")
end

=begin
2013-06-27
2013-06-26
2013-06-25
2013-06-24
2013-06-23
2013-06-22
2013-06-21
2013-06-20
2013-06-19
2013-06-18
2013-06-17
=end